/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.blocks;

public enum EnumSides
{
	NORTH(0, 0, -1, "north"),
	SOUTH(0, 0, 1, "south"),
	EAST(1, 0, 0, "east"),
	WEST(-1, 0, 0, "west"),
	TOP(0, 1, 0, "top", "up"),
	BOTTOM(0, -1, 0, "down", "bottom"),
	UNDEFINED(0, 0, 0, "none", "undefined");

	static
	{
		NORTH.setOpposite(SOUTH);
		SOUTH.setOpposite(NORTH);
		EAST.setOpposite(WEST);
		WEST.setOpposite(EAST);
		BOTTOM.setOpposite(TOP);
		TOP.setOpposite(BOTTOM);
	}
	private int x, y, z;
	private String[] aliases;
	private EnumSides opposite;

	EnumSides(int x, int y, int z, String... aliases)
	{
		this.aliases = aliases;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public String[] getAliases()
	{
		return this.aliases;
	}

	public int getTranslationX()
	{
		return this.x;
	}

	public int getTranslationY()
	{
		return this.y;
	}

	public int getTranslationZ()
	{
		return this.z;
	}

	public static EnumSides fromString(String a)
	{
		for(EnumSides side : EnumSides.values())
			if(side.getAliases() != null)
				for(String alias : side.getAliases())
					if(alias.equals(a))
						return side;
		return UNDEFINED;
	}

	void setOpposite(EnumSides opposite)
	{
		this.opposite = opposite;
	}

	public EnumSides opposite()
	{
		return this.opposite;
	}
}
