/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.blocks;

import fr.land.utils.color.Color4f;
import fr.land.utils.math.AxisAlignedBB;

public class Block
{
	public boolean isSolid = true;
	public boolean isVisible = true;

	public String blockName = "";

	public Color4f color;

	public int textureId;

	// ********************************************************************************************************************************************

	public Block()
	{
		this(new Color4f(0, 1, 0, 1));
	}

	public Block(Color4f color)
	{
		this.color = color;
		this.textureId = 0;
	}

	// ********************************************************************************************************************************************

	public int[] getTexturePosition()
	{
		return new int[] {this.textureId % 8, this.textureId / 8};
	}
	
	public float[] blockDataFront(float x, float y, float z)
	{
		int s = 1;

		return new float[] {
				x, y, z,				this.color.r * 0.9f, this.color.g * 0.9f, this.color.b * 0.9f, this.color.a,
				x + s, y, z, 			this.color.r * 0.9f, this.color.g * 0.9f, this.color.b * 0.9f, this.color.a,
				x + s, y + s, z, 		this.color.r * 0.9f, this.color.g * 0.9f, this.color.b * 0.9f, this.color.a,
				x, y + s, z, 			this.color.r * 0.9f, this.color.g * 0.9f, this.color.b * 0.9f, this.color.a,
		};
	}
	
	public float[] blockDataBack(float x, float y, float z)
	{
		int s = 1;
		
		return new float[] {
				x, y, z + s, 			this.color.r * 0.9f, this.color.g * 0.9f, this.color.b * 0.9f, this.color.a,
				x, y + s, z + s, 		this.color.r * 0.9f, this.color.g * 0.9f, this.color.b * 0.9f, this.color.a,
				x + s, y + s, z + s,	this.color.r * 0.9f, this.color.g * 0.9f, this.color.b * 0.9f, this.color.a,
				x + s, y, z + s, 		this.color.r * 0.9f, this.color.g * 0.9f, this.color.b * 0.9f, this.color.a,
		};
	}
	
	public float[] blockDataLeft(float x, float y, float z)
	{
		int s = 1;
		
		return new float[] {
				x, y, z, 				this.color.r * 0.8f, this.color.g * 0.8f, this.color.b * 0.8f, this.color.a,
				x, y + s, z, 			this.color.r * 0.8f, this.color.g * 0.8f, this.color.b * 0.8f, this.color.a,
				x, y + s, z + s, 		this.color.r * 0.8f, this.color.g * 0.8f, this.color.b * 0.8f, this.color.a,
				x, y, z + s, 			this.color.r * 0.8f, this.color.g * 0.8f, this.color.b * 0.8f, this.color.a,
		};
	}
	
	public float[] blockDataRight(float x, float y, float z)
	{
		int s = 1;
		
		return new float[] {
				x + s, y + s, z, 		this.color.r * 0.8f, this.color.g * 0.8f, this.color.b * 0.8f, this.color.a,
				x + s, y, z, 			this.color.r * 0.8f, this.color.g * 0.8f, this.color.b * 0.8f, this.color.a,
				x + s, y, z + s, 		this.color.r * 0.8f, this.color.g * 0.8f, this.color.b * 0.8f, this.color.a,
				x + s, y + s, z + s, 	this.color.r * 0.8f, this.color.g * 0.8f, this.color.b * 0.8f, this.color.a,
		};
	}
	
	public float[] blockDataDown(float x, float y, float z)
	{
		int s = 1;
		
		return new float[] {
				x + s, y, z, 			this.color.r * 0.7f, this.color.g * 0.7f, this.color.b * 0.7f, this.color.a,
				x, y, z, 				this.color.r * 0.7f, this.color.g * 0.7f, this.color.b * 0.7f, this.color.a,
				x, y, z + s, 			this.color.r * 0.7f, this.color.g * 0.7f, this.color.b * 0.7f, this.color.a,
				x + s, y, z + s, 		this.color.r * 0.7f, this.color.g * 0.7f, this.color.b * 0.7f, this.color.a, 
		};
	}
	
	public float[] blockDataUp(float x, float y, float z)
	{
		int s = 1;
		
		return new float[] {
				
				x, y + s, z, 			this.color.r * 1.0f, this.color.g * 1.0f, this.color.b * 1.0f, this.color.a,
				x + s, y + s, z, 		this.color.r * 1.0f, this.color.g * 1.0f, this.color.b * 1.0f, this.color.a, 
				x + s, y + s, z + s, 	this.color.r * 1.0f, this.color.g * 1.0f, this.color.b * 1.0f, this.color.a,
				x, y + s, z + s, 		this.color.r * 1.0f, this.color.g * 1.0f, this.color.b * 1.0f, this.color.a,
		};
	}

	// ********************************************************************************************************************************************

	public boolean isSolid()
	{
		return this.isSolid;
	}

	public boolean isVisible()
	{
		return this.isVisible;
	}

	public AxisAlignedBB getBoundingBox(int x, int y, int z)
	{
		return new AxisAlignedBB(x, y, z, 1, 1, 1);
	}

	// ********************************************************************************************************************************************

}
