/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.blocks;

import java.util.HashMap;

import fr.land.utils.color.Color4f;

public class BlockList
{

	public static Block[] blockList = new Block[4096];
	public static HashMap<Integer, String> blockName = new HashMap<Integer, String>();

	// ********************************************************************************************************************************************

	public static void register(Block block, String name)
	{
		System.out.println("Register " + name + " to ID : " + BlockList.blockName.size());
		BlockList.blockList[BlockList.blockName.size()] = block;
		BlockList.blockName.put(BlockList.blockName.size(), name);
	}

	// ********************************************************************************************************************************************

	public static final Block air = new BlockAir();
	public static final Block stone = new Block(new Color4f(0, 1, 0,1));
//	public static Block air, grass;

	public static void init()
	{
		BlockList.register(BlockList.air, "Air");
		BlockList.register(BlockList.stone, "Stone");
//		BlockList.register(BlockList.grass, "Grass");
	}
}
