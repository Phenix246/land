/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.core;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import fr.land.blocks.BlockList;
import fr.land.client.renderer.DisplayManager;
import fr.land.client.renderer.Shader;
import fr.land.client.renderer.texture.Texture;
import fr.land.entities.player.EntityPlayer;
import fr.land.network.PacketList;
import fr.land.utils.crash.CrashReport;
import fr.land.utils.math.Vector3f;
import fr.land.utils.resources.AssetLoader;
import fr.land.utils.resources.ClasspathSimpleResourceLoader;
import fr.land.utils.thread.GameThread;
import fr.land.utils.thread.ThreadClientInput;
import fr.land.world.World;

public class Land
{
	// Instance
	private static Land land;

	// Boolean for debug option
	public boolean isDebug = false;

	// Boolean for running game
	private boolean isRunning = false;

	// All the thread of the game
	public GameThread clientInput, clientRender, serverInput, serverUpdate;

	// Player
	public EntityPlayer player;

	// Worlds
	public World world;

	// Texture files
	public Texture blockTexture;

	// Utils
	public static final float FRAME_CAP = 1200;
	public static final float TICK_CAP = 60;
	private AssetLoader assetsLoader;

	// ********************************************************************************************************************************************

	// Main method (start by java)
	public static void main(String[] args)
	{
		Land.land = new Land();
		Land.land.CheckFiles();
		Land.land.initDisplay();
		Land.land.initOpenGL();
		Land.land.launchTread();

		// Game
		Land.land.initGame();
		Land.land.launchGame();
		Land.land.startGame();
	}

	// ********************************************************************************************************************************************

	// Check files
	public void CheckFiles()
	{

	}

	// Launch All the Threads
	public void launchTread()
	{
		// Initialize threads
		// Thread.currentThread().setName("Thread-Client-Main");
		// this.clientInput = new ThreadClientInput("Thread-Client-Input");
		// this.clientRender = new ThreadClientRender("Thread-Client-Render");
		// this.serverInput = new ThreadServerInput("Thread-Server-Input");
		// this.serverUpdate = new ThreadServerUpdate("Thread-Server-Update");

		// Start threads
		// this.clientRender.start();
		// this.serverInput.start();
		// this.serverUpdate.start();
	}

	// Initialize Display and Camera
	public void initDisplay()
	{
		// Create window
		DisplayManager.create(1280, 720);
		DisplayManager.setTitle("Land");

	}

	// Initialize OpenGL
	public void initOpenGL()
	{
		GL11.glEnable(GL11.GL_DEPTH_TEST);

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);

		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_FRONT);

	}

	// ********************************************************************************************************************************************

	// Get Game Instance
	public static Land getLand()
	{
		return Land.land;
	}

	public void initGame()
	{
		this.assetsLoader = new AssetLoader(new ClasspathSimpleResourceLoader("assets"));

		// Texture
		this.blockTexture = new Texture("/assets/textures/blocks.png");
		// Blocks
		BlockList.init();

		// Items

		// Entities

		// Worlds
		this.world = new World();

		// Packets
		PacketList.init();

	}

	public void launchGame()
	{
		this.player = new EntityPlayer(new Vector3f(5, 32, 5), new Vector3f(0, 180, 0));
		this.player.rot.y = 180;
		this.world.addEntity(this.player);
	}

	// Start Game
	public void startGame()
	{
		this.isRunning = true;
		this.loop();
	}

	// Stop Game
	public void stopGame()
	{
		this.isRunning = false;;
		this.closeGame();
	}

	// Close Game
	public void closeGame()
	{
		DisplayManager.destroy();

		// Set isRunning to false
		// this.clientInput.isRunning = false;
		// this.clientRender.isRunning = false;
		// this.serverInput.isRunning = false;
		// this.serverUpdate.isRunning = false;

		// Close programs
		System.exit(0);
	}

	// ********************************************************************************************************************************************

	// Main loop
	public void loop()
	{
		long lastTickTime = System.nanoTime();
		long lastRenderTime = System.nanoTime();

		// Duration for a tick
		double tickTime = 1000000000.0 / Land.TICK_CAP;

		// Duration for a frame render
		double renderTime = 1000000000.0 / Land.FRAME_CAP;

		// Ticks and Frames counter
		int ticks = 0;
		int frames = 0;

		long timer = System.currentTimeMillis();

		while(this.isRunning)
		{
			// Test if user want to close the window
			if(DisplayManager.isClosed())
				this.stopGame();

			// Update game
			if(System.nanoTime() - lastTickTime > tickTime)
			{
				lastTickTime += tickTime;
				this.update(ticks);
				ticks++;
			}
			// Render Game
			else if(System.nanoTime() - lastRenderTime > renderTime)
			{
				lastRenderTime += renderTime;
				this.render();
				frames++;
			}
			// Wait
			else
				try
				{
					Thread.sleep(1);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}

			if(System.currentTimeMillis() - timer > 1000)
			{
				timer += 1000;
				// if(this.isDebug)
					System.out.println(ticks + " tps, " + frames + " fps");
				ticks = 0;
				frames = 0;
			}
		}
	}

	// Update method
	private synchronized void update(int tick)
	{
		// Get inputs
		this.clientInput = new ThreadClientInput("Thread-Client-Input");
		if(!this.clientInput.isAlive())
			this.clientInput.start();

		// Update world
		this.world.update(tick);
	}

	// Render method
	private synchronized void render()
	{
		// Update screen
		DisplayManager.update();
		DisplayManager.resize();

		// Clear screen
		DisplayManager.prepare();

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(70.0f, (float)Display.getWidth() / (float)Display.getHeight(), 0.05f, 1000.0f);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();

		// Move Camera
		GL11.glRotatef(this.player.rot.x, 1, 0, 0);
		GL11.glRotatef(this.player.rot.y, 0, 1, 0);
		GL11.glRotatef(this.player.rot.z, 0, 0, 1);
		GL11.glTranslatef(-this.player.pos.x, -this.player.pos.y - this.player.eyesHeight, -this.player.pos.z);

		// Sharders
		Shader.MAIN.bind();

		// Render world
		this.world.render();
	}

	// ********************************************************************************************************************************************

	// Forces a crash with given crash report
	public void crash(CrashReport crashReport)
	{
		crashReport.printStack();
		this.closeGame();
		System.exit(-1);
	}

	// Return the game's Assets Loader
	public AssetLoader getAssetsLoader()
	{
		return this.assetsLoader;
	}
}
