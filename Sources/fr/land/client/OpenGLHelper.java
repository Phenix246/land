/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.client;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL32;

public class OpenGLHelper
{
	/**
	 * Loads a OpenGL Texture from given image
	 */
	// public static Texture loadTexture(BufferedImage img)
	// {
	// return OpenGLHelper.loadTexture(img, GL11.GL_NEAREST);
	// }
	//
	// public static Texture loadTexture(BufferedImage img, int filter)
	// {
	// int w = img.getWidth();
	// int h = img.getHeight();
	// ByteBuffer pixelBuf = ImageUtils.getPixels(img);
	// return new Texture(w, h, pixelBuf, filter);
	// }

	/**
	 * Loads given texture
	 */
	// public static Texture loadTexture(AbstractResource resource) throws
	// IOException
	// {
	// BufferedImage img = ImageUtils.loadImage(resource);
	// return OpenGLHelper.loadTexture(img);
	// }

	private static HashMap<Integer, String> capNamesMap = new HashMap<Integer, String>();

	/**
	 * Loads all GL_* fields with their respective values (used mostly for crash
	 * reports)
	 */
	public static void loadCapNames()
	{
		OpenGLHelper.loadCapNames(GL11.class);
		OpenGLHelper.loadCapNames(GL12.class);
		OpenGLHelper.loadCapNames(GL13.class);
		OpenGLHelper.loadCapNames(GL14.class);
		OpenGLHelper.loadCapNames(GL15.class);
		OpenGLHelper.loadCapNames(GL20.class);
		OpenGLHelper.loadCapNames(GL21.class);
		OpenGLHelper.loadCapNames(GL30.class);
		OpenGLHelper.loadCapNames(GL31.class);
		OpenGLHelper.loadCapNames(GL32.class);
	}

	/**
	 * Loads all GL_* fields with their respective values from given class (used
	 * mostly for crash reports)
	 */
	private static void loadCapNames(Class<?> glClass)
	{
		Field[] fields = glClass.getFields();
		for(Field field : fields)
			if(Modifier.isPublic(field.getModifiers()) && Modifier.isStatic(field.getModifiers()) && Modifier.isFinal(field.getModifiers()))
				try
				{
					if(field.getType() == Integer.TYPE)
					{
						int value = (Integer)field.get(null);
						if(!OpenGLHelper.capNamesMap.containsKey(value))
							OpenGLHelper.capNamesMap.put(value, field.getName());
					}
				}
				catch(IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch(IllegalAccessException e)
				{
					e.printStackTrace();
				}
	}

	/**
	 * Returns OpenGL version
	 */
	public static String getOpenGLVersion()
	{
		String version = GL11.glGetString(GL11.GL_VERSION);

		// Remove Driver Info
		if(version.indexOf(' ') != -1)
			version = version.substring(0, version.indexOf(' '));
		return version;
	}

	/**
	 * Returns OpenGL vendor
	 */
	public static String getOpenGLVendor()
	{
		return GL11.glGetString(GL11.GL_VENDOR);
	}

	/**
	 * Returns OpenGL renderer info
	 */
	public static String getOpenGLRendererInfo()
	{
		return GL11.glGetString(GL11.GL_RENDERER);
	}

	/**
	 * Returns OpenGL capability name
	 */
	public static String getCapName(int cap)
	{
		if(!OpenGLHelper.capNamesMap.containsKey(cap))
			return "" + cap;
		return OpenGLHelper.capNamesMap.get(cap);
	}
}
