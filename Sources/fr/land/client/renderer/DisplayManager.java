/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.client.renderer;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class DisplayManager
{
	public static int width, height;

	public static void setTitle(String title)
	{
		Display.setTitle(title);
	}

	public static void create(int width, int height)
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.setResizable(true);
			Display.create();
		}
		catch(LWJGLException e)
		{
			e.printStackTrace();
		}
	}

	public static void prepare()
	{
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
	}

	public static void update()
	{
		Display.update();
	}

	public static boolean isClosed()
	{
		return Display.isCloseRequested();
	}

	public static void destroy()
	{
		Display.destroy();
	}

	public static void resize()
	{
		if(Display.wasResized())
		{
			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
			DisplayManager.width = Display.getWidth();
			DisplayManager.height = Display.getHeight();
		}
	}
}
