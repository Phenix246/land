///*******************************************************************************
// * Copyright (c) 2015, Phenix246
// *
// * This work is made available under the terms of the Creative Commons Attribution License:
// * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
// * Contact the author for use the sources
// *
// * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
// * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
// * Contacter l'auteur pour utiliser les sources
// *
// * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
// * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
// * Contactar al autor para utilizar las fuentes
// *
// ******************************************************************************/
//package fr.land.client.renderer;
//
//import org.lwjgl.input.Keyboard;
//import org.lwjgl.input.Mouse;
//
//import fr.land.core.Land;
//import fr.land.entities.player.EntityPlayer;
//import fr.land.utils.math.Vector3f;
//
//public class Camera
//{
//	public static float mouseSpeed = 0.5f;
//	public static float moveSpeed = 0.5f;
//
//	private float fov;
//	private float zNear;
//	private float zFar;
//
////	private Vector3f position;
////	private Vector3f rotation = new Vector3f();
//
//	public Vector3f camPos = new Vector3f(64, 32, 32);
//	public Vector3f camRot = new Vector3f();
//	
//	public Camera(Vector3f position)
//	{
//		this.camPos = position;
//	}
//
//	public void input()
//	{
//		EntityPlayer player = Land.getLand().player;
//		player.rot.x -= Mouse.getDY() * Camera.mouseSpeed;
//		player.rot.y += Mouse.getDX() * Camera.mouseSpeed;
//
//		if(player.rot.x >= 90)
//			player.rot.x = 90;
//		if(player.rot.x <= -90)
//			player.rot.x = -90;
//
//		if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL))
//			Camera.moveSpeed = 0.8f;
//		else
//			Camera.moveSpeed = 0.1f;
//
//		if(Keyboard.isKeyDown(Keyboard.KEY_Z))
//		{
//			player.pos.z -= Math.cos(Math.toRadians(player.rot.y)) * player.speed;
//			player.pos.x += Math.sin(Math.toRadians(player.rot.y)) * player.speed;
//		}
//		if(Keyboard.isKeyDown(Keyboard.KEY_S))
//		{
//			player.pos.z += Math.cos(Math.toRadians(player.rot.y)) * player.speed;
//			player.pos.x -= Math.sin(Math.toRadians(player.rot.y)) * player.speed;
//		}
//		if(Keyboard.isKeyDown(Keyboard.KEY_Q))
//		{
//			player.pos.z += Math.cos(Math.toRadians(player.rot.y + 90)) * player.speed;
//			player.pos.x -= Math.sin(Math.toRadians(player.rot.y + 90)) * player.speed;
//		}
//		if(Keyboard.isKeyDown(Keyboard.KEY_D))
//		{
//			player.pos.z -= Math.cos(Math.toRadians(player.rot.y + 90)) * player.speed;
//			player.pos.x += Math.sin(Math.toRadians(player.rot.y + 90)) * player.speed;
//		}
//		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
//			player.pos.y += player.speed;
//		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
//			player.pos.y -= player.speed;
//
//		// this.camRot.x -= Mouse.getDY() * Camera.mouseSpeed;
//		// this.camRot.y += Mouse.getDX() * Camera.mouseSpeed;
//		//
//		// if(this.camRot.x >= 90)
//		// this.camRot.x = 90;
//		// if(this.camRot.x <= -90)
//		// this.camRot.x = -90;
//		//
//		// if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL))
//		// Camera.moveSpeed = 0.8f;
//		// else
//		// Camera.moveSpeed = 0.1f;
//		//
//		// if(Keyboard.isKeyDown(Keyboard.KEY_Z))
//		// {
//		// this.camPos.z -= Math.cos(Math.toRadians(this.camRot.y)) *
//		// Camera.moveSpeed;
//		// this.camPos.x += Math.sin(Math.toRadians(this.camRot.y)) *
//		// Camera.moveSpeed;
//		// }
//		// if(Keyboard.isKeyDown(Keyboard.KEY_S))
//		// {
//		// this.camPos.z += Math.cos(Math.toRadians(this.camRot.y)) *
//		// Camera.moveSpeed;
//		// this.camPos.x -= Math.sin(Math.toRadians(this.camRot.y)) *
//		// Camera.moveSpeed;
//		// }
//		// if(Keyboard.isKeyDown(Keyboard.KEY_Q))
//		// {
//		// this.camPos.z += Math.cos(Math.toRadians(this.camRot.y + 90)) *
//		// Camera.moveSpeed;
//		// this.camPos.x -= Math.sin(Math.toRadians(this.camRot.y + 90)) *
//		// Camera.moveSpeed;
//		// }
//		// if(Keyboard.isKeyDown(Keyboard.KEY_D))
//		// {
//		// this.camPos.z -= Math.cos(Math.toRadians(this.camRot.y + 90)) *
//		// Camera.moveSpeed;
//		// this.camPos.x += Math.sin(Math.toRadians(this.camRot.y + 90)) *
//		// Camera.moveSpeed;
//		// }
//		// if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
//		// this.camPos.y += Camera.moveSpeed;
//		// if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
//		// this.camPos.y -= Camera.moveSpeed;
//	}
//
////	public Vector3f getForward()
////	{
////		Vector3f r = new Vector3f();
////
////		Vector3f rot = new Vector3f(this.rotation);
////
////		float cosY = (float)Math.cos(Math.toRadians(rot.getY() - 90));
////		float sinY = (float)Math.sin(Math.toRadians(rot.getY() - 90));
////		float cosP = (float)Math.cos(Math.toRadians(-rot.getX()));
////		float sinP = (float)Math.sin(Math.toRadians(-rot.getX()));
////
////		// Euler Angles
////		r.setX(cosY * cosP);
////		r.setY(sinP);
////		r.setZ(sinY * cosP);
////
////		r.normalize();
////
////		return new Vector3f(r);
////	}
////
////	public Vector3f getBack()
////	{
////		return new Vector3f(this.getForward().mul(-1));
////	}
////
////	public Vector3f getRight()
////	{
////		Vector3f rot = new Vector3f(this.rotation);
////
////		Vector3f r = new Vector3f();
////		r.setX((float)Math.cos(Math.toRadians(rot.getY())));
////		r.setZ((float)Math.sin(Math.toRadians(rot.getY())));
////		r.normalize();
////
////		return new Vector3f(r);
////	}
////
////	public Vector3f getLeft()
////	{
////		return new Vector3f(this.getRight().mul(-1));
////	}
////
////	public Vector3f getPosition()
////	{
////		return this.position;
////	}
////
////	public void setPosition(Vector3f position)
////	{
////		this.position = position;
////	}
////
////	public Vector3f getRotation()
////	{
////		return this.rotation;
////	}
////
////	public void setRotation(Vector3f rotation)
////	{
////		this.rotation = rotation;
////	}
// }
