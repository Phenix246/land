/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.client.renderer;

import org.lwjgl.opengl.GL11;

public class RendererHelper
{
	public static void renderCube(float x, float y, float z, float s)
	{
		GL11.glVertex3f(x, y, z);
		GL11.glVertex3f(x + s, y, z);
		GL11.glVertex3f(x + s, y + s, z);
		GL11.glVertex3f(x, y + s, z);

		GL11.glVertex3f(x, y, z + s);
		GL11.glVertex3f(x, y + s, z + s);
		GL11.glVertex3f(x + s, y + s, z + s);
		GL11.glVertex3f(x + s, y, z + s);

		GL11.glVertex3f(x, y, z);
		GL11.glVertex3f(x, y + s, z);
		GL11.glVertex3f(x, y + s, z + s);
		GL11.glVertex3f(x, y, z + s);

		GL11.glVertex3f(x + s, y + s, z);
		GL11.glVertex3f(x + s, y, z);
		GL11.glVertex3f(x + s, y, z + s);
		GL11.glVertex3f(x + s, y + s, z + s);

		GL11.glVertex3f(x + s, y, z);
		GL11.glVertex3f(x, y, z);
		GL11.glVertex3f(x, y, z + s);
		GL11.glVertex3f(x + s, y, z + s);

		GL11.glVertex3f(x, y + s, z);
		GL11.glVertex3f(x + s, y + s, z);
		GL11.glVertex3f(x + s, y + s, z);
		GL11.glVertex3f(x, y + s, z + s);

	}
}
