/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.client.renderer.texture;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class Texture
{
	private int width, height;
	private int id;

	public Texture(String path)
	{
		this.load(path);
	}

	private void load(String path)
	{
		BufferedImage image = null;
		try
		{
			System.out.println(Texture.class.getResource(path).getPath().replaceAll("%20", " "));
			image = ImageIO.read(Texture.class.getResource(path));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		this.width = image.getWidth();
		this.height = image.getHeight();

		int[] pixels = new int[this.width * this.height];

		image.getRGB(0, 0, this.width, this.height, pixels, 0, this.width);

		ByteBuffer buffer = BufferUtils.createByteBuffer(this.width * this.height * 4);

		for(int y = 0; y < this.width; y++)
			for(int x = 0; x < this.height; x++)
			{
				int i = pixels[x + y * this.height];
				buffer.put((byte)(i >> 16 & 0xFF));
				buffer.put((byte)(i >> 8 & 0xFF));
				buffer.put((byte)(i & 0xFF));
				buffer.put((byte)(i >> 24 & 0xFF));
			}

		buffer.flip();

		this.id = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.id);

		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, this.width, this.height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);

	}

	public void bind()
	{
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.id);
	}

	public void unbind()
	{
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
}
