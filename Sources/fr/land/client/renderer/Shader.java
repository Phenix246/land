/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.client.renderer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import fr.land.utils.math.Vector3f;

public class Shader
{
	public static final Shader MAIN = new Shader("/shaders/main.vert", "/shaders/main.frag");

	public int program;

	public Shader(String vertex, String fragment)
	{
		this.program = GL20.glCreateProgram();

		if(this.program == GL11.GL_FALSE)
		{
			System.err.println("Shader program error");
			System.exit(1);
		}

		this.createShader(this.loadShader(vertex), GL20.GL_VERTEX_SHADER);
		this.createShader(this.loadShader(fragment), GL20.GL_FRAGMENT_SHADER);

		GL20.glLinkProgram(this.program);
		GL20.glValidateProgram(this.program);
	}

	private void createShader(String source, int type)
	{
		int shader = GL20.glCreateShader(type);
		if(shader == GL11.GL_FALSE)
		{
			System.err.println("Shader error: " + shader);
			System.exit(1);
		}
		GL20.glShaderSource(shader, source);
		GL20.glCompileShader(shader);
		if(GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
		{
			System.err.println(GL20.glGetShaderInfoLog(shader, 2048));
			System.exit(1);
		}
		GL20.glAttachShader(this.program, shader);
	}

	private String loadShader(String input)
	{
		String r = "";

		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(Shader.class.getResourceAsStream(input)));
			String buffer = "";
			while((buffer = reader.readLine()) != null)
				r += buffer + "\n";
			reader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return r;
	}

	public void setUniform(String name, float v)
	{
		GL20.glUniform1f(GL20.glGetUniformLocation(this.program, name), v);
	}

	public void setUniform(String name, Vector3f v)
	{
		GL20.glUniform3f(GL20.glGetUniformLocation(this.program, name), v.getX(), v.getY(), v.getZ());
	}

	public void bind()
	{
		GL20.glUseProgram(this.program);
	}
}
