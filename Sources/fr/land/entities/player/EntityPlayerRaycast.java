/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.entities.player;

import java.util.ArrayList;

import fr.land.utils.math.Vector3f;
import fr.land.world.World;

public class EntityPlayerRaycast
{
	private ArrayList<Vector3f> points;
	private EntityPlayer player;
	private World world;
	
	public EntityPlayerRaycast(EntityPlayer player)
	{
		this.player = player;
		this.world = player.world;
		this.points = new ArrayList<Vector3f>();
		for(int i = 0; i < 10 * 16; i++)
			this.points.add(new Vector3f());
	}

	private void update()
	{
		int i = 0;
		for(Vector3f v : this.points)
		{
			Vector3f pos = this.player.getDirection().copy().mul(i / 16f);
			v.set(pos);;
			i++;
		}
	}
	
	public Vector3f getBlock()
	{
		for(Vector3f v : this.points)
		{
			int x = (int) v.x;
			int y = (int) v.y;
			int z = (int) v.z;
			boolean block = this.world.getBlock(x, y, z) != null;

			if(block)
				return new Vector3f(x, y, z);
		}
		return null;
	}
}
