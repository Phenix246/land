/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.entities.player;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import fr.land.entities.EntityLiving;
import fr.land.utils.math.Vector3f;

public class EntityPlayer extends EntityLiving
{

	private float xDir, yDir, zDir;

	public float speed;
	public float eyesHeight = 1.8f;

	// ********************************************************************************************************************************************

	public EntityPlayer(Vector3f pos)
	{
		super(pos);
		this.moveX = pos.x;
		this.moveY = pos.y;
		this.moveZ = pos.z;
	}

	public EntityPlayer(Vector3f pos, Vector3f rot)
	{
		super(pos, rot);
		this.moveX = pos.x;
		this.moveY = pos.y;
		this.moveZ = pos.z;
	}

	// ********************************************************************************************************************************************

	public void input()
	{
		this.xDir = 0;
		this.yDir = 0;
		this.zDir = 0;

		// move
		this.rot.x -= Mouse.getDY() * this.speed;
		this.rot.y += Mouse.getDX() * this.speed;

		if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL))
			this.speed = 0.8f;
		else
			this.speed = 0.3f;

		if(Keyboard.isKeyDown(Keyboard.KEY_Z))
			this.zDir = -this.speed;
		if(Keyboard.isKeyDown(Keyboard.KEY_S))
			this.zDir = this.speed;
		if(Keyboard.isKeyDown(Keyboard.KEY_Q))
			this.xDir = -this.speed;
		if(Keyboard.isKeyDown(Keyboard.KEY_D))
			this.xDir = this.speed;
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
			this.yDir = this.speed;
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
			this.yDir = -this.speed;

		this.moveX += this.xDir * (float)Math.cos(Math.toRadians(this.rot.y)) - this.zDir * (float)Math.sin(Math.toRadians(this.rot.y));
		this.moveY += this.yDir;
		this.moveZ += this.zDir * (float)Math.cos(Math.toRadians(this.rot.y)) + this.xDir * (float)Math.sin(Math.toRadians(this.rot.y));

		this.move(this.moveX, this.moveY, this.moveZ);
	}

	@Override
	public void update(int tick)
	{
		super.update(tick);
	}

	@Override
	public void render()
	{

	}

	// ********************************************************************************************************************************************

}
