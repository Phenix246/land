/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.entities;

import fr.land.utils.math.AxisAlignedBB;
import fr.land.utils.math.Vector3f;
import fr.land.world.Chunk;
import fr.land.world.World;

public class Entity
{
	public World world;
	public Vector3f pos, rot;
	public float speed;

	protected float moveX, moveY, moveZ;

	// ********************************************************************************************************************************************

	public Entity(Vector3f pos)
	{
		this(pos, new Vector3f());
	}

	public Entity(Vector3f pos, Vector3f rot)
	{
		this.pos = pos;
		this.rot = rot;
	}

	public Entity init(World world)
	{
		this.world = world;
		return this;
	}

	// ********************************************************************************************************************************************

	public void render()
	{

	}

	public void update(int tick)
	{
		// if(tick == 0)
		// System.out.println("x: " + this.pos.x + " y: " + this.pos.y + " z: "
		// + this.pos.z);
	}

	// ********************************************************************************************************************************************

	public void move(float xa, float ya, float za)
	{
		if(!this.isColliding(xa, this.pos.y, this.pos.z) && !this.isOut(xa, 1, 1))
			this.pos.x = xa;
		else
			this.moveX = this.pos.x;
		if(!this.isColliding(this.pos.x, ya, this.pos.z) && !this.isOut(1, ya, 1))
			this.pos.y = ya;
		else
			this.moveY = this.pos.y;
		if(!this.isColliding(this.pos.x, this.pos.y, za) && !this.isOut(1, 1, za))
			this.pos.z = za;
		else
			this.moveZ = this.pos.z;
	}


	public boolean isColliding(float xa, float ya, float za)
	{
		AxisAlignedBB box = this.world.getBoundingBox((int)xa, (int)ya, (int)za);
		AxisAlignedBB entity = this.getBoundingBox(xa, ya, za);

		if(entity.isCollide(box))
			return true;
		if(box.isCollide(entity))
			return true;
		return false;
	}

	public boolean isOut(float x, float y, float z)
	{
		if(x < 0 || x >= World.SIZE * Chunk.SIZE)
			return true;
		if(y < 0 || y >= World.HEIGHT * Chunk.SIZE)
			return true;
		if(z < 0 || z >= World.SIZE * Chunk.SIZE)
			return true;

		return false;
	}


	public Vector3f getDirection()
	{
		Vector3f r = new Vector3f();
		Vector3f rot = new Vector3f(this.rot);

		float cosY = (float)Math.cos(Math.toRadians(rot.y - 90));
		float sinY = (float)Math.sin(Math.toRadians(rot.y - 90));
		float cosP = (float)Math.cos(Math.toRadians(-rot.x));
		float sinP = (float)Math.sin(Math.toRadians(-rot.x));

		// Euler Angles
		r.setX(cosY * cosP);
		r.setY(sinP);
		r.setZ(sinY * cosP);

		r.normalize();

		return new Vector3f(r);
	}

	public AxisAlignedBB getBoundingBox()
	{
		return new AxisAlignedBB(this.pos.x, this.pos.y, this.pos.z, 1, 2, 1);
	}

	public AxisAlignedBB getBoundingBox(float x, float y, float z)
	{
		return new AxisAlignedBB(x, y, z, 1, 2, 1);
	}

}
