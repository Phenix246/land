/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.events;


public enum EnumEventPriority
{
	/*
	 * Priority of event listeners, listeners will be sorted with respect to
	 * this priority level.
	 * 
	 * Note: Due to using a ArrayList in the ListenerList, these need to stay in
	 * a contiguous index starting at 0. {Default ordinal}
	 */
	HIGHEST, // First to execute
	HIGH, NORMAL, LOW, LOWEST // Last to execute
	;
}
