/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.events;

import java.lang.reflect.Method;
import java.util.HashMap;

import fr.land.utils.Log;

public class EventBus implements IEventExceptionHandler
{

	private HashMap<Object, String> listeners = new HashMap<>();
	private IEventExceptionHandler exceptionHandler;

	public EventBus()
	{
		this.exceptionHandler = this;
	}

	public void register(Object target)
	{
		if(this.listeners.containsKey(target) || target == null)
			return;

		this.listeners.put(target, target.getClass().getSimpleName());
	}

	public void unregister(Object object)
	{
		this.listeners.remove(object);
	}

	public boolean post(Event event)
	{
		try
		{
			Object[] list = this.listeners.keySet().toArray();
			for(EnumEventPriority priority : EnumEventPriority.values())
				for(Object instance : list)
					for(Method method : instance.getClass().getMethods())
						if(method.isAnnotationPresent(SubscribeEvent.class))
							if(method.getAnnotation(SubscribeEvent.class).priority().equals(priority))
								method.invoke(instance, event);
		}
		catch(Throwable throwable)
		{
			this.exceptionHandler.handleException(this, event, throwable);
		}
		return event.isCancelable() ? event.isCanceled() : false;
	}

	@Override
	public void handleException(EventBus bus, Event event, Throwable throwable)
	{
		Log.error("name: " + event.getClass().getSimpleName() + " -> " + throwable.getMessage());
	}
}
