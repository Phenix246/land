package fr.land.utils.color;

import org.lwjgl.opengl.GL11;

public class Color3f
{

	public float r, g, b;

	public Color3f(float r, float g, float b)
	{
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public Color3f(Color3f color)
	{
		this(color.r, color.g, color.b);
	}

	public Color3f(int r, int g, int b)
	{
		this(r / 255f, g / 255f, b / 255f);
	}

	public void change(float r, float g, float b)
	{
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public void drawWith()
	{
		GL11.glColor3f(this.r, this.g, this.b);
	}

	public void drawWithout()
	{
		GL11.glColor3f(1f, 1f, 1f);
	}

	public Color4f toColor4f(float a)
	{
		return new Color4f(this.r, this.g, this.b, a);
	}

}
