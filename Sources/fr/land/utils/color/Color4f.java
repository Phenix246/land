/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.color;

import org.lwjgl.opengl.GL11;

public class Color4f
{

	public float r, g, b, a;

	public Color4f(float r, float g, float b, float a)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public Color4f(float r, float g, float b)
	{
		this(r, g, b, 1);
	}

	public Color4f(Color3f color)
	{
		this(color.r, color.g, color.b, 1f);
	}

	public Color4f(Color4f color)
	{
		this(color.r, color.g, color.b, color.a);
	}

	public void change(float r, float g, float b, float a)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public void drawWith()
	{
		GL11.glColor4f(this.r, this.g, this.b, this.a);
	}

	public void drawWithout()
	{
		GL11.glColor4f(1f, 1f, 1f, 1f);
	}

	public Color3f toColor4f()
	{
		return new Color3f(this.r, this.g, this.b);
	}

}
