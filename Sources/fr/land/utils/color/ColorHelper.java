package fr.land.utils.color;

public class ColorHelper
{
	public static float getRedFromHex(int color)
	{
		return (color >> 16 & 0xFF) / 255F;
	}

	public static float getGreenFromHex(int color)
	{
		return (color >> 16 & 0xFF) / 255F;
	}

	public static float getBlueFromHex(int color)
	{
		return (color >> 16 & 0xFF) / 255F;
	}

	public static float getAlphaFromHex(int color)
	{
		return (color >> 16 & 0xFF) / 255F;
	}

	public static Color3f mix(Color3f c1, Color3f c2)
	{
		float r, g, b;

		r = (c1.r + c2.r) / 2;
		g = (c1.g + c2.g) / 2;
		b = (c1.b + c2.b) / 2;

		return new Color3f(r, g, b);
	}

	public static Color4f mix(Color4f c1, Color4f c2)
	{
		float r, g, b, a;

		r = (c1.r + c2.r) / 2;
		g = (c1.g + c2.g) / 2;
		b = (c1.b + c2.b) / 2;
		a = (c1.a + c2.a) / 2;

		return new Color4f(r, g, b, a);
	}
}
