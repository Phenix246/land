/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.math;

public class AxisAlignedBB
{
	public float x, y, z, width, height, depth;

	public AxisAlignedBB(Vector3f pos, Vector3f size)
	{
		this.x = pos.x;
		this.y = pos.y;
		this.z = pos.z;
		this.width = size.x;
		this.height = size.y;
		this.depth = size.z;
	}

	public AxisAlignedBB(float x, float y, float z, float width, float height, float depth)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.depth = depth;
	}

	public void expand(float x1, float y1, float z1, float x2, float y2, float z2)
	{
		this.x -= x1;
		this.y -= y1;
		this.z -= z1;
		this.width += x2;
		this.height += y2;
		this.depth += y2;
	}

	public void reduce(float x1, float y1, float z1, float x2, float y2, float z2)
	{
		this.x += x1;
		this.y += y1;
		this.z += z1;
		this.width -= x2;
		this.height -= y2;
		this.depth -= y2;
	}

	public boolean isCollide(float x, float y, float z)
	{
		 if (x >= this.x 
		    && x < this.x + this.width
		    && y >= this.y 
		    && y < this.y + this.height
		    && z >= this.z 
		    && z < this.z + this.depth)
			return true;
		else
			return false;
	}

	public boolean isCollide(AxisAlignedBB box)
	{
		if(this.x >= box.x + box.width // trop à droite
				|| this.x + this.width <= box.x // trop à gauche
				|| this.y >= box.y + box.height // trop en bas
				|| this.y + this.height <= box.y // trop en haut
				|| this.z >= box.z + box.depth // trop derrière
				|| this.z + this.depth <= box.z) // trop devant
			return false;
		else
			return true;
	}

	public String getString()
	{
		return "Axis aligned Bounding Box - x: " + this.x + " y: " + this.y + " z: " + this.z + " width: " + this.width + " height: " + this.height + " depth: " + this.depth;
	}
}
