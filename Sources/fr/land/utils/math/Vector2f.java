/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.math;

public class Vector2f
{
	public float x, y;

	// ********************************************************************************************************************************************

	public Vector2f()
	{
		this(0, 0);
	}

	public Vector2f(Vector2f v)
	{
		this(v.x, v.y);
	}

	public Vector2f(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	// ********************************************************************************************************************************************

	public float length()
	{
		return (float)Math.sqrt(this.x * this.x + this.y * this.y);
	}

	public Vector2f normalize()
	{
		this.x /= this.length();
		this.y /= this.length();

		return this;
	}

	public Vector2f add(Vector2f vec)
	{
		this.x += vec.getX();
		this.y += vec.getY();

		return this;
	}

	public Vector2f sub(Vector2f vec)
	{
		this.x -= vec.getX();
		this.y -= vec.getY();

		return this;
	}

	public Vector2f mul(Vector2f vec)
	{
		this.x *= vec.getX();
		this.y *= vec.getY();

		return this;
	}

	public Vector2f div(Vector2f vec)
	{
		this.x /= vec.getX();
		this.y /= vec.getY();

		return this;
	}

	public Vector2f add(float v)
	{
		this.x += v;
		this.y += v;

		return this;
	}

	public Vector2f sub(float v)
	{
		this.x -= v;
		this.y -= v;

		return this;
	}

	public Vector2f mul(float v)
	{
		this.x *= v;
		this.y *= v;

		return this;
	}

	public Vector2f div(float v)
	{
		this.x /= v;
		this.y /= v;

		return this;
	}

	// ********************************************************************************************************************************************
	public float getX()
	{
		return this.x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public Vector2f addX(float v)
	{
		this.x += v;
		return this;
	}

	public Vector2f subX(float v)
	{
		this.x -= v;
		return this;
	}

	// ********************************************************************************************************************************************
	public float getY()
	{
		return this.y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public Vector2f addY(float v)
	{
		this.y += v;
		return this;
	}

	public Vector2f subY(float v)
	{
		this.y -= v;
		return this;
	}
}
