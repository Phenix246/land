/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.math;

public class Vector3f
{
	public float x, y, z;

	// ********************************************************************************************************************************************

	public Vector3f()
	{
		this(0, 0, 0);
	}

	public Vector3f(Vector3f v)
	{
		this(v.x, v.y, v.z);
	}

	public Vector3f(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	// ********************************************************************************************************************************************

	public float length()
	{
		return (float)Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
	}

	public Vector3f normalize()
	{
		this.x /= this.length();
		this.y /= this.length();
		this.z /= this.length();

		return this;
	}

	public Vector3f add(Vector3f vec)
	{
		this.x += vec.getX();
		this.y += vec.getY();
		this.z += vec.getZ();

		return this;
	}

	public Vector3f sub(Vector3f vec)
	{
		this.x -= vec.getX();
		this.y -= vec.getY();
		this.z -= vec.getZ();

		return this;
	}

	public Vector3f mul(Vector3f vec)
	{
		this.x *= vec.getX();
		this.y *= vec.getY();
		this.z *= vec.getZ();

		return this;
	}

	public Vector3f div(Vector3f vec)
	{
		this.x /= vec.getX();
		this.y /= vec.getY();
		this.z /= vec.getZ();

		return this;
	}

	public Vector3f add(float v)
	{
		this.x += v;
		this.y += v;
		this.z += v;

		return this;
	}

	public Vector3f sub(float v)
	{
		this.x -= v;
		this.y -= v;
		this.z -= v;

		return this;
	}

	public Vector3f mul(float v)
	{
		this.x *= v;
		this.y *= v;
		this.z *= v;

		return this;
	}

	public Vector3f div(float v)
	{
		this.x /= v;
		this.y /= v;
		this.z /= v;

		return this;
	}

	public Vector3f copy()
	{
		return this;
	}

	public void set(Vector3f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
		this.z = vec.getZ();
	}

	// ********************************************************************************************************************************************

	public float getX()
	{
		return this.x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public Vector3f addX(float v)
	{
		this.x += v;
		return this;
	}

	public Vector3f subX(float v)
	{
		this.x -= v;
		return this;
	}

	// ********************************************************************************************************************************************

	public float getY()
	{
		return this.y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public Vector3f addY(float v)
	{
		this.y += v;
		return this;
	}

	public Vector3f subY(float v)
	{
		this.y -= v;
		return this;
	}

	// ********************************************************************************************************************************************

	public float getZ()
	{
		return this.z;
	}

	public void setZ(float z)
	{
		this.z = z;
	}

	public Vector3f addZ(float v)
	{
		this.z += v;
		return this;
	}

	public Vector3f subZ(float v)
	{
		this.z -= v;
		return this;
	}


}
