/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.land.core.Land;
import fr.land.utils.Log.NonLoggable;
import fr.land.utils.crash.CrashReport;

@NonLoggable
public class Log
{
	private static final Logger log = LoggerFactory.getLogger("OurLand");

	/**
	 * If a method or a class has this annotation on, the logger will ignore
	 * them when searching for the caller.
	 */
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface NonLoggable
	{}

	public static boolean showCaller = true;
	public static boolean useFullClassNames = false;

	@NonLoggable
	public static void message(String msg)
	{
		Log.message(msg, true);
	}

	@NonLoggable
	private static String format(String rawMessage)
	{
		if(Log.showCaller)
			return "[In " + Log.getCaller() + "] " + rawMessage;
		return rawMessage;
	}

	@NonLoggable
	private static String getCaller()
	{
		StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		if(elems != null)
			elementsIteration: for(int i = 1; i < elems.length; i++)
			{
				StackTraceElement elem = elems[i];
				if(elem.getClassName().contains(Log.class.getCanonicalName()))
					continue;
				if(elem.getClassName().contains(org.slf4j.Logger.class.getCanonicalName()))
					continue;
				if(elem.getClassName().contains(Logger.class.getCanonicalName()))
					continue;
				try
				{
					Class<?> c = Class.forName(elem.getClassName());
					if(c.isAnnotationPresent(NonLoggable.class))
						continue;
					for(Method method : c.getDeclaredMethods())
						if(method.getName().equals(elem.getMethodName()))
							if(method.isAnnotationPresent(NonLoggable.class))
								continue elementsIteration;

					String name = null;
					if(Log.useFullClassNames)
						name = c.getCanonicalName();
					else
						name = c.getSimpleName();
					String s = name + "." + elem.getMethodName() + ":" + elem.getLineNumber();
					return s;
				}
				catch(ClassNotFoundException e)
				{
					e.printStackTrace();
				}
			}
		return "Unknown source";
	}

	@NonLoggable
	public static void message(String msg, boolean format)
	{
		String formated = Log.format(msg);
		Log.log.info(formated);
	}

	@NonLoggable
	public static void error(String msg)
	{
		Log.error(msg, true);
	}

	@NonLoggable
	public static void error(String msg, boolean format)
	{
		String formated = Log.format(msg);
		Log.log.error(formated);
	}

	@NonLoggable
	public static void fatal(String msg)
	{
		if(Land.getLand() != null)
			Land.getLand().crash(new CrashReport(msg));
		else
		{
			new CrashReport(msg).printStack();
			System.exit(-2);
		}
	}

	@NonLoggable
	public static void debug(String msg)
	{
		String formated = Log.format(msg);
		Log.log.debug(formated);
	}

	@NonLoggable
	public static void error(String formated, Throwable t)
	{
		Log.log.error(formated, t);
	}

	@NonLoggable
	public static void debug(String formated, Throwable t)
	{
		Log.log.debug(formated, t);
	}

	@NonLoggable
	public static void message(String formated, Throwable t)
	{
		Log.log.info(formated, t);
	}

	@NonLoggable
	public static void trace(String formated, Throwable t)
	{
		Log.log.trace(formated, t);
	}
}
