/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.handler;

public class StringUtils
{
	public static String removeSpacesAtStart(String line)
	{
		while(!line.isEmpty() && line.startsWith(" "))
			line = line.substring(1);
		return line;
	}

	public static String createCorrectedFileName(String name)
	{
		StringBuffer buffer = new StringBuffer();
		for(int i = 0; i < name.length(); i++)
		{
			char c = name.charAt(i);
			if(c == ':' || c == '?' || c == '/' || c == '\\')
				c = '_';
			buffer.append(c);
		}

		return buffer.toString();
	}

	public static boolean isEmpty(final CharSequence cs)
	{
		return cs == null || cs.length() == 0;
	}

	public static boolean isNotEmpty(final CharSequence cs)
	{
		return !StringUtils.isEmpty(cs);
	}

	public static boolean isAnyEmpty(CharSequence... css)
	{
		if(ArrayUtils.isEmpty(css))
			return true;
		for(CharSequence cs : css)
			if(StringUtils.isEmpty(cs))
				return true;
		return false;
	}

	public static boolean isNoneEmpty(CharSequence... css)
	{
		return !StringUtils.isAnyEmpty(css);
	}

	public static boolean isBlank(final CharSequence cs)
	{
		int strLen;
		if(cs == null || (strLen = cs.length()) == 0)
			return true;
		for(int i = 0; i < strLen; i++)
			if(Character.isWhitespace(cs.charAt(i)) == false)
				return false;
		return true;
	}

	public static boolean isNotBlank(final CharSequence cs)
	{
		return !StringUtils.isBlank(cs);
	}

	public static boolean isAnyBlank(CharSequence... css)
	{
		if(ArrayUtils.isEmpty(css))
			return true;
		for(CharSequence cs : css)
			if(StringUtils.isBlank(cs))
				return true;
		return false;
	}

	public static boolean isNoneBlank(CharSequence... css)
	{
		return !StringUtils.isAnyBlank(css);
	}

}
