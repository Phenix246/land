/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.handler;

import java.util.ArrayList;

public class ArrayUtils
{
	public static <E> ArrayList<E> newList()
	{
		return new ArrayList<E>();
	}

	public static String[] trim(String[] split)
	{
		java.util.List<String> strings = ArrayUtils.newList();
		for(String s : split)
			if(s != null && !s.trim().equals(""))
				strings.add(s);
		return strings.toArray(new String[0]);
	}

	public static String sum(String[] tokens, int start, int length, String separator)
	{
		StringBuffer buffer = new StringBuffer();
		for(int i = start; i < length + start; i++)
		{
			buffer.append(tokens[i]);
			if(i != length + start - 1)
				buffer.append(separator);
		}
		return buffer.toString();
	}

	private static int getSize(Object[]... array)
	{
		int size = 0;
		for(int i = 0; i < array.length; i++)
			size += array[i].length;
		return size;
	}

	public static float[] fusion(float[]... array)
	{
		float[] na = new float[ArrayUtils.getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		return na;
	}

	public static double[] fusion(double[]... array)
	{
		double[] na = new double[ArrayUtils.getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		return na;
	}

	public static boolean[] fusion(boolean[]... array)
	{
		boolean[] na = new boolean[ArrayUtils.getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		return na;
	}

	public static byte[] fusion(byte[]... array)
	{
		byte[] na = new byte[ArrayUtils.getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		return na;
	}

	public static short[] fusion(short[]... array)
	{
		short[] na = new short[ArrayUtils.getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		return na;
	}

	public static int[] fusion(int[]... array)
	{
		int[] na = new int[ArrayUtils.getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		return na;
	}

	public static long[] fusion(long[]... array)
	{
		long[] na = new long[ArrayUtils.getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		return na;
	}

	public static Object[] fusion(Object[]... array)
	{
		Object[] na = new Object[ArrayUtils.getSize(array)];
		int k = 0;
		for(int i = 0; i < array.length; i++)
			for(int j = 0; j < array[i].length; j++)
			{
				na[k] = array[i][j];
				k++;
			}
		return na;
	}

	public static boolean isEmpty(final Object[] array)
	{
		return array == null || array.length == 0;
	}

	public static boolean isEmpty(final long[] array)
	{
		return array == null || array.length == 0;
	}

	public static boolean isEmpty(final int[] array)
	{
		return array == null || array.length == 0;
	}

	public static boolean isEmpty(final short[] array)
	{
		return array == null || array.length == 0;
	}

	public static boolean isEmpty(final char[] array)
	{
		return array == null || array.length == 0;
	}

	public static boolean isEmpty(final byte[] array)
	{
		return array == null || array.length == 0;
	}

	public static boolean isEmpty(final double[] array)
	{
		return array == null || array.length == 0;
	}

	public static boolean isEmpty(final float[] array)
	{
		return array == null || array.length == 0;
	}

	public static boolean isEmpty(final boolean[] array)
	{
		return array == null || array.length == 0;
	}

	public static <T> boolean isNotEmpty(final T[] array)
	{
		return array != null && array.length != 0;
	}

	public static boolean isNotEmpty(final long[] array)
	{
		return array != null && array.length != 0;
	}

	public static boolean isNotEmpty(final int[] array)
	{
		return array != null && array.length != 0;
	}

	public static boolean isNotEmpty(final short[] array)
	{
		return array != null && array.length != 0;
	}

	public static boolean isNotEmpty(final char[] array)
	{
		return array != null && array.length != 0;
	}

	public static boolean isNotEmpty(final byte[] array)
	{
		return array != null && array.length != 0;
	}

	public static boolean isNotEmpty(final double[] array)
	{
		return array != null && array.length != 0;
	}

	public static boolean isNotEmpty(final float[] array)
	{
		return array != null && array.length != 0;
	}

	public static boolean isNotEmpty(final boolean[] array)
	{
		return array != null && array.length != 0;
	}
}
