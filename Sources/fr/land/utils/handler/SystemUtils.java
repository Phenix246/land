/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.handler;

import java.io.File;

import fr.land.utils.EnumOS;

public class SystemUtils
{

	private static File gameFolder;

	public static EnumOS getOS()
	{
		String os = System.getProperty("os.name").toLowerCase();
		if(os.contains("win"))
			return EnumOS.WINDOWS;
		if(os.contains("sunos") || os.contains("solaris"))
			return EnumOS.SOLARIS;
		if(os.contains("unix"))
			return EnumOS.LINUX;
		if(os.contains("linux"))
			return EnumOS.LINUX;
		if(os.contains("mac"))
			return EnumOS.MACOSX;
		return EnumOS.UNKNOWN;
	}

	public static String getUserName()
	{
		return System.getProperty("user.name");
	}

	/**
	 * Returns the folder where game data is saved
	 */
	public static File getGameFolder()
	{
		if(SystemUtils.gameFolder == null)
		{
			String appdata = System.getenv("APPDATA");
			if(appdata != null)
				SystemUtils.gameFolder = new File(appdata, ".ourland");
			else
				SystemUtils.gameFolder = new File(System.getProperty("user.home"), ".ourland");
		}
		return SystemUtils.gameFolder;
	}

	public static void deleteRecursivly(File file)
	{
		if(file.isDirectory())
		{
			File[] list = file.listFiles();
			if(list != null)
				for(File f : list)
				{
					SystemUtils.deleteRecursivly(f);
					f.delete();
				}
		}
		file.delete();
	}

	public static void setGameFolder(File file)
	{
		SystemUtils.gameFolder = file;
	}
}
