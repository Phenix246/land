/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.auth;

import java.util.UUID;

import fr.land.utils.handler.StringUtils;

public class GameProfile
{
	private final UUID id;
	private final String name;
	private boolean legacy;

	/**
	 * Constructs a new Game Profile with the specified ID and name.
	 * <p />
	 * Either ID or name may be null/empty, but at least one must be filled.
	 *
	 * @param id
	 *            Unique ID of the profile
	 * @param name
	 *            Display name of the profile
	 * @throws java.lang.IllegalArgumentException
	 *             Both ID and name are either null or empty
	 */
	public GameProfile(UUID id, String name)
	{
		if(id == null && StringUtils.isBlank(name))
			throw new IllegalArgumentException("Name and ID cannot both be blank");

		this.id = id;
		this.name = name;
	}

	/**
	 * Gets the unique ID of this game profile.
	 * <p />
	 * This may be null for partial profile data if constructed manually.
	 *
	 * @return ID of the profile
	 */
	public UUID getId()
	{
		return this.id;
	}

	/**
	 * Gets the display name of this game profile.
	 * <p />
	 * This may be null for partial profile data if constructed manually.
	 *
	 * @return Name of the profile
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Checks if this profile is complete.
	 * <p />
	 * A complete profile has no empty fields. Partial profiles may be
	 * constructed manually and used as input to methods.
	 *
	 * @return True if this profile is complete (as opposed to partial)
	 */
	public boolean isComplete()
	{
		return this.id != null && StringUtils.isNotBlank(this.getName());
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || this.getClass() != o.getClass())
			return false;

		GameProfile that = (GameProfile)o;

		if(this.id != null ? !this.id.equals(that.id) : that.id != null)
			return false;
		if(this.name != null ? !this.name.equals(that.name) : that.name != null)
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = this.id != null ? this.id.hashCode() : 0;
		result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return "";// new ToStringBuilder(this).append("id",
					// this.id).append("name", this.name).append("properties",
					// properties).append("legacy", this.legacy).toString();
	}

	public boolean isLegacy()
	{
		return this.legacy;
	}
}
