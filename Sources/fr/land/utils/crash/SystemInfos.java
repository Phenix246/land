/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.crash;

public class SystemInfos implements CrashInfos
{

	@Override
	public String getInfos()
	{
		String osName = System.getProperty("os.name");
		String arch = System.getProperty("os.arch");
		String osVersion = System.getProperty("os.version");
		String jvmName = System.getProperty("java.vm.name");
		String jvmVendor = System.getProperty("java.vm.vendor");
		String javaVendor = System.getProperty("java.vendor");
		String javaVersion = System.getProperty("java.version");

		return CrashInfos.SECTION_START + " System info " + CrashInfos.SECTION_END + "\n\tOperating System: " + osName + " (" + arch + ")" + " version " + osVersion + "\n\tJava Version: " + javaVersion + ", " + javaVendor + "\n\tVM Version: " + jvmName + ", " + jvmVendor;
	}

}