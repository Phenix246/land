/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.crash;

import java.nio.charset.Charset;

import fr.land.core.Land;
import fr.land.utils.resources.ResourceLocation;

public class CrashReport
{
	private static String[] comments = new String[] {null, null, "Well, this was a disappointment.", "I'm sorry Dave. I think I can't let you do that", "Here, have a gift http://xkcd.com/953/ ", "This computer is on fiiiiiiiiiiiiiiiiiiiire!"};

	public static class UndefinedException extends Exception
	{

		public UndefinedException(String message)
		{
			super(message);
		}

		private static final long serialVersionUID = 3352250643266742630L;
	}

	private Throwable exception;

	public CrashReport(String message)
	{
		this(new UndefinedException(message).fillInStackTrace());
	}

	public CrashReport(Throwable throwable)
	{
		this.exception = throwable;
		try
		{
			if(Land.getLand() != null && CrashReport.comments[0] == null)
			{
				CrashReport.comments[0] = new String(Land.getLand().getAssetsLoader().getResource(new ResourceLocation("ourcraft", "text/crackedFloppy.ascii")).getData(), Charset.forName("utf-8")).replace("\n       -jglrxavpok", "");
				CrashReport.comments[1] = new String(Land.getLand().getAssetsLoader().getResource(new ResourceLocation("ourcraft", "text/deadFace.ascii")).getData(), Charset.forName("utf-8")).replace("\n       -jglrxavpok", "");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void printStack()
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append(CrashInfos.SECTION_START + " Crash " + CrashInfos.SECTION_END + "\n");
		String comment = this.generateRandomComment();
		buffer.append(comment + "\n");
		buffer.append("\n" + this.exception.getClass().getCanonicalName());
		StackTraceElement[] stackTrace = this.exception.getStackTrace();
		if(this.exception.getLocalizedMessage() != null)
			buffer.append(": " + this.exception.getLocalizedMessage());
		else if(this.exception.getMessage() != null)
			buffer.append(": " + this.exception.getMessage());
		buffer.append("\n");
		if(stackTrace != null && stackTrace.length > 0)
			for(StackTraceElement elem : stackTrace)
				buffer.append("\tat " + elem.toString() + "\n");
		else
			buffer.append("\t**** Stack Trace is empty ****");
		buffer.append(CrashInfos.SECTION_START + " Game " + CrashInfos.SECTION_END + "\n\tName: OurCraft\n");
		this.add(buffer, new DateInfos());
		this.add(buffer, new SystemInfos());
		if(Land.getLand() != null)
		{
			this.add(buffer, new OpenALInfos());
			this.add(buffer, new OpenGLInfos());
		}
		System.err.println(buffer.toString());
	}

	private void add(StringBuffer buffer, CrashInfos infos)
	{
		try
		{
			buffer.append(infos.getInfos() + "\n");
		}
		catch(Exception e)
		{
			;
		}
	}

	private String generateRandomComment()
	{
		int index = (int)Math.floor(Math.random() * CrashReport.comments.length);
		while(CrashReport.comments[index] == null)
			index = (int)Math.floor(Math.random() * CrashReport.comments.length);
		return CrashReport.comments[index];
	}
}
