/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.crash;

import org.lwjgl.openal.AL10;

public class OpenALInfos implements CrashInfos
{

	@Override
	public String getInfos()
	{
		String s = CrashInfos.SECTION_START + " OpenAL " + CrashInfos.SECTION_END;
		s += "\n\tVersion: " + AL10.alGetString(AL10.AL_VERSION);
		s += "\n\tVendor: " + AL10.alGetString(AL10.AL_VENDOR);
		return s;
	}
}
