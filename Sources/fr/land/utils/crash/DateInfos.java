/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.crash;

import java.util.Calendar;

public class DateInfos implements CrashInfos
{

	private static String[] days = new String[7];
	private static String[] months = new String[12];

	private static Calendar calendar;

	static
	{
		DateInfos.calendar = Calendar.getInstance();
		DateInfos.days[Calendar.MONDAY - 1] = "Monday";
		DateInfos.days[Calendar.TUESDAY - 1] = "Tuesday";
		DateInfos.days[Calendar.WEDNESDAY - 1] = "Wednesday";
		DateInfos.days[Calendar.THURSDAY - 1] = "Thursday";
		DateInfos.days[Calendar.FRIDAY - 1] = "Friday";
		DateInfos.days[Calendar.SATURDAY - 1] = "Saturday";
		DateInfos.days[Calendar.SUNDAY - 1] = "Sunday";

		DateInfos.months[Calendar.JANUARY] = "January";
		DateInfos.months[Calendar.FEBRUARY] = "February";
		DateInfos.months[Calendar.MARCH] = "March";
		DateInfos.months[Calendar.APRIL] = "April";
		DateInfos.months[Calendar.MAY] = "May";
		DateInfos.months[Calendar.JUNE] = "June";
		DateInfos.months[Calendar.JULY] = "July";
		DateInfos.months[Calendar.AUGUST] = "August";
		DateInfos.months[Calendar.SEPTEMBER] = "September";
		DateInfos.months[Calendar.OCTOBER] = "October";
		DateInfos.months[Calendar.NOVEMBER] = "November";
		DateInfos.months[Calendar.DECEMBER] = "December";
	}

	public static long getTime()
	{
		return System.currentTimeMillis();
	}

	public static String getTimeAsText()
	{
		String second = "" + DateInfos.calendar.get(Calendar.SECOND);
		String minute = "" + DateInfos.calendar.get(Calendar.MINUTE);
		String hour = "" + DateInfos.calendar.get(Calendar.HOUR_OF_DAY);
		String day = DateInfos.days[DateInfos.calendar.get(Calendar.DAY_OF_WEEK) - 1];
		String month = DateInfos.months[DateInfos.calendar.get(Calendar.MONTH)];
		String year = "" + DateInfos.calendar.get(Calendar.YEAR);

		if(second.length() == 1)
			second = "0" + second;

		if(minute.length() == 1)
			minute = "0" + minute;

		if(hour.length() == 1)
			hour = "0" + hour;

		return month + ", " + day + " " + year + " at " + hour + "h" + minute + " and " + second + " seconds";
	}

	@Override
	public String getInfos()
	{
		return CrashInfos.SECTION_START + " Date " + CrashInfos.SECTION_END + "\n\t" + DateInfos.getTimeAsText();
	}

}
