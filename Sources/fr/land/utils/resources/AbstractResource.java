/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.resources;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

public abstract class AbstractResource
{
	private ResourceLocation location;
	private InputStream inputStream;
	private ResourceLoader loader;

	public AbstractResource(ResourceLocation location, InputStream inputStream, ResourceLoader loader)
	{
		this.location = location;
		this.inputStream = inputStream;
		this.loader = loader;
	}

	public ResourceLoader getLoader()
	{
		return this.loader;
	}

	public InputStream getInputStream()
	{
		return this.inputStream;
	}

	public ResourceLocation getResourceLocation()
	{
		return this.location;
	}

	@Override
	public boolean equals(Object o)
	{
		if(o instanceof AbstractResource)
		{
			AbstractResource res = (AbstractResource)o;

			return res.getResourceLocation().getFullPath().equals(this.getResourceLocation().getFullPath()) && res.getLoader().getClass() == this.getLoader().getClass();
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		final int BASE = 17;
		final int MULTIPLIER = 31;

		int result = BASE;
		result = MULTIPLIER * result + this.getResourceLocation().getSection().hashCode();
		result = MULTIPLIER * result + this.getResourceLocation().getPath().hashCode();

		return result;
	}

	public File asFile()
	{
		throw new IllegalArgumentException("Impossible to retrieve resource as file");
	}

	/**
	 * Returns raw data from resource
	 */
	public abstract byte[] getData();

	public URL getURL() throws MalformedURLException
	{
		throw new UnsupportedOperationException();
	}

	public String readContent() throws UnsupportedEncodingException
	{
		return new String(this.getData(), "UTF-8");
	}
}
