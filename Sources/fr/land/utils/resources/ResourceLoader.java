/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.resources;

import java.io.IOException;
import java.util.List;

public abstract class ResourceLoader
{
	public ResourceLoader()
	{}

	// Loads a Resource from given location
	public abstract AbstractResource getResource(ResourceLocation location) throws IOException;

	// Returns List of Resources from given location
	public abstract List<AbstractResource> getAllResources(ResourceLocation location) throws IOException;

	// Checks if Resource exists

	public abstract boolean doesResourceExists(ResourceLocation location);

	public AbstractResource getResourceOrCreate(ResourceLocation resourceLocation) throws IOException
	{
		return this.getResource(resourceLocation);
	}
}
