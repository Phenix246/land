/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.resources;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ClasspathSimpleResourceLoader extends ResourceLoader
{

	private HashMap<String, AbstractResource> resources = new HashMap<String, AbstractResource>();
	private String base;

	public ClasspathSimpleResourceLoader()
	{
		this("");
	}

	public ClasspathSimpleResourceLoader(String base)
	{
		if(!base.endsWith("/"))
			base += "/";
		this.base = base;
	}

	@Override
	public AbstractResource getResource(ResourceLocation location) throws IOException
	{
		if(!this.isLoaded(location))
		{
			InputStream stream = ClasspathSimpleResourceLoader.class.getResourceAsStream("/" + this.base + location.getFullPath());
			if(stream != null)
			{
				URL url = ClasspathSimpleResourceLoader.class.getResource("/" + this.base + location.getFullPath());
				this.resources.put(location.getFullPath(), new SimpleResource(location, url, stream, this));
			}
			else
				throw new FileNotFoundException("Resource /" + this.base + location.getFullPath() + " not found.");
		}
		if(!this.isLoaded(location))
			throw new FileNotFoundException("Resource /" + this.base + location.getFullPath() + " not found.");
		return this.resources.get(location.getFullPath());
	}

	public boolean isLoaded(ResourceLocation location)
	{
		return this.resources.containsKey(location.getFullPath());
	}

	@Override
	public List<AbstractResource> getAllResources(ResourceLocation location) throws IOException
	{
		return Arrays.asList(this.getResource(location));
	}

	@Override
	public boolean doesResourceExists(ResourceLocation location)
	{
		try
		{
			if(ClasspathSimpleResourceLoader.class.getResource("/" + this.base + location.getFullPath()) != null)
				return true;
		}
		catch(Exception e)
		{
			return false;
		}
		return false;
	}

}