/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.resources;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;

public class SimpleResource extends AbstractResource
{

	private byte[] bytes;
	private URL url;

	public SimpleResource(ResourceLocation location, URL url, InputStream inputStream, ResourceLoader loader)
	{
		super(location, inputStream, loader);
		this.url = url;
	}

	@Override
	public byte[] getData()
	{
		if(this.bytes == null)
			try
			{
				byte[] buffer = new byte[65565];
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				int i;
				while((i = this.getInputStream().read(buffer, 0, buffer.length)) != -1)
					baos.write(buffer, 0, i);
				baos.flush();
				baos.close();
				this.bytes = baos.toByteArray();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		return this.bytes;
	}

	@Override
	public File asFile()
	{
		throw new IllegalArgumentException("Impossible to retrieve simple resource as file");
	}

	@Override
	public URL getURL()
	{
		return this.url;
	}

}
