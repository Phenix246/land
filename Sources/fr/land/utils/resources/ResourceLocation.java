/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.resources;

import fr.land.utils.handler.ArrayUtils;

public class ResourceLocation
{

	private String section;
	private String path;
	private ResourceLocation parent;

	public ResourceLocation(String location)
	{
		if(location.contains(":"))
		{
			this.section = location.split(":")[0];
			this.path = location.split(":")[1];
		}
		else
		{
			this.section = "";
			this.path = location;
		}
	}

	public ResourceLocation(String section, String path)
	{
		this.section = section;
		this.path = path;
	}

	public ResourceLocation(ResourceLocation parent, String path)
	{
		this(parent.getSection(), parent.getPath() + "/" + path);
		this.parent = parent;
	}

	public String getSection()
	{
		return this.section;
	}

	public String getPath()
	{
		return this.path;
	}

	public String getFullPath()
	{
		String prefix = "";
		if(!this.section.equals(""))
			prefix = this.section + "/";
		return prefix + this.path;
	}

	public String getExtension()
	{
		if(this.path.contains("/"))
		{
			String tokens[] = this.path.split("/");
			String token = tokens[tokens.length - 1];
			if(token.contains("."))
			{
				tokens = token.split("\\.");
				return tokens[tokens.length - 1];
			}
			return token;
		}
		else if(this.path.contains("."))
		{
			String tokens[] = this.path.split("\\.");
			return tokens[tokens.length - 1];
		}
		return this.path;
	}

	/**
	 * Returns direct parent
	 */
	public ResourceLocation getDirectParent()
	{
		if(this.parent == null)
			if(this.path.contains("/"))
			{
				String tokens[] = this.path.split("/");
				this.parent = new ResourceLocation(this.section, ArrayUtils.sum(tokens, 0, tokens.length - 1, "/"));

			}
			else
				this.parent = new ResourceLocation(this.section);
		return this.parent;
	}

	public String getName()
	{
		if(this.path.contains("/"))
		{
			String tokens[] = this.path.split("/");
			return tokens[tokens.length - 1];
		}
		else
			return this.path;
	}

	@Override
	public boolean equals(Object o)
	{
		if(o instanceof ResourceLocation)
		{
			ResourceLocation res = (ResourceLocation)o;
			return res.getFullPath().equals(this.getFullPath());
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		return this.getFullPath().hashCode();
	}

	public ResourceLocation getChild(String child)
	{
		return new ResourceLocation(this.getFullPath() + "/" + child);
	}
}
