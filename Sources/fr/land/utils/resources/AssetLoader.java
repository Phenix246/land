/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.resources;

import java.io.IOException;
import java.util.List;

public class AssetLoader extends ResourceLoader
{

	private ClasspathSimpleResourceLoader mainLoader;
	private ResourceLoader resourcePackLoader;

	public AssetLoader(ClasspathSimpleResourceLoader mainLoader)
	{
		this(mainLoader, null);
	}

	public AssetLoader(ClasspathSimpleResourceLoader mainLoader, ResourceLoader resourcePackLoader)
	{
		this.mainLoader = mainLoader;
		this.resourcePackLoader = resourcePackLoader;
	}

	@Override
	public AbstractResource getResource(ResourceLocation location) throws IOException
	{
		if(this.resourcePackLoader != null)
			if(this.resourcePackLoader.doesResourceExists(location))
				return this.resourcePackLoader.getResource(location);
		return this.mainLoader.getResource(location);
	}

	@Override
	public List<AbstractResource> getAllResources(ResourceLocation location) throws IOException
	{
		if(this.resourcePackLoader != null)
			if(this.resourcePackLoader.doesResourceExists(location))
				return this.resourcePackLoader.getAllResources(location);
		return this.mainLoader.getAllResources(location);
	}

	@Override
	public boolean doesResourceExists(ResourceLocation location)
	{
		if(this.resourcePackLoader != null)
			return this.mainLoader.doesResourceExists(location) || this.resourcePackLoader.doesResourceExists(location);
		return this.mainLoader.doesResourceExists(location);
	}

	public void setResourcePackLoader(ResourceLoader loader)
	{
		this.resourcePackLoader = loader;
	}

	public ResourceLoader getResourcePackLoader()
	{
		return this.resourcePackLoader;
	}

}
