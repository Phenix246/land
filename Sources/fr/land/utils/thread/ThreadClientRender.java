/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.thread;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import fr.land.client.renderer.DisplayManager;
import fr.land.core.Land;

public class ThreadClientRender extends GameThread
{

	public Land land;

	public ThreadClientRender(String name)
	{
		super(name);
	}

	@Override
	public void run()
	{
		// Create window
		DisplayManager.create(1280, 720);
		DisplayManager.setTitle("Land");

		this.land = Land.getLand();

		// Initialize Mouse and Keyboard
		try
		{
			Mouse.create();
			Keyboard.create();
		}
		catch(LWJGLException e)
		{
			e.printStackTrace();
		}

		while(this.isRunning)
		{
			// Update Display
			DisplayManager.update();

			// Test if the user want to close the game
			if(DisplayManager.isClosed())
				Land.getLand().closeGame();

			this.land.clientInput = new ThreadClientInput("Thread-Client-Input");
			if(!this.land.clientInput.isAlive())
				this.land.clientInput.start();

		}
	}
}
