/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.thread;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import fr.land.core.Land;

public class ThreadClientInput extends GameThread
{

	public Land land;

	public ThreadClientInput(String name)
	{
		super(name);
		this.land = Land.getLand();
	}

	@Override
	public void run()
	{
		// Get command from user to unGrab Mouse
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
			Mouse.setGrabbed(false);

		// Get command from user to Grab Mouse
		if(Mouse.isButtonDown(0) && !Mouse.isGrabbed())
			Mouse.setGrabbed(true);

		// If Mouse in nit Grabbed don't get other inputs
		if(!Mouse.isGrabbed())
			return;

		// Players input
		this.land.player.input();

		// Player commands
		if(Keyboard.isKeyDown(Keyboard.KEY_F3))
			this.land.isDebug = !this.land.isDebug;

		this.interrupt();
	}
}
