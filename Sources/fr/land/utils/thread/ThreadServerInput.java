/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.utils.thread;

public class ThreadServerInput extends GameThread
{

	public ThreadServerInput(String name)
	{
		super(name);
	}

	@Override
	public void run()
	{
		while(this.isRunning)
		{
			try
			{
				Thread.sleep(500L);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
}
