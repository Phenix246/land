/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.network;

import io.netty.buffer.*;

public class ByteBufUtils
{

	public static byte[] getAsArray(ByteBuf msg)
	{
		byte[] buffer = new byte[msg.readableBytes()];
		msg.readBytes(buffer);
		return buffer;
	}

	public static void writeString(ByteBuf buffer, String s)
	{
		buffer.writeInt(s.length());
		for(char c : s.toCharArray())
		{
			buffer.writeChar(c);
		}
	}

	public static String readString(ByteBuf buffer)
	{
		int l = buffer.readInt();
		String result = "";
		for(int i = 0; i < l; i++ )
		{
			result += buffer.readChar();
		}
		return result;
	}

	public static void writeBytes(ByteBuf buffer, byte[] bytes)
	{
		buffer.writeInt(bytes.length);
		buffer.writeBytes(bytes);
	}

	public static byte[] readBytes(ByteBuf buffer)
	{
		int l = buffer.readInt();
		byte[] result = new byte[l];
		buffer.readBytes(result);
		return result;
	}

}
