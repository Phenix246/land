/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.network;

import io.netty.buffer.*;
import io.netty.channel.*;
import io.netty.handler.codec.*;

public class PacketEncoder extends MessageToByteEncoder<NettyPacket>
{

    @Override
    protected void encode(ChannelHandlerContext arg0, NettyPacket arg1, ByteBuf arg2) throws Exception
    {
        arg2.writeInt(arg1.id);
        arg2.writeInt(arg1.side.ordinal());
        arg2.writeInt(arg1.payload.writerIndex());
        arg2.writeBytes(arg1.payload);
    }
}
