/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.network;

import io.netty.buffer.*;
import io.netty.channel.*;
import io.netty.util.concurrent.*;

public class ChannelHelper
{

    public static void writeAndFlush(AbstractPacket packet, ChannelHandlerContext ctx)
    {
        writeAndFlush(packet, ctx.channel());
    }

    public static void writeAndFlush(AbstractPacket packet, Channel channel)
    {
        int id = PacketRegistry.getPacketId(packet.getClass());
        NetworkSide side = PacketRegistry.getPacketSide(packet.getClass());
        ByteBuf buffer = channel.alloc().buffer();
        packet.encodeInto(buffer);
        NettyPacket nettyPacket = new NettyPacket(id, buffer, side);
        channel.writeAndFlush(nettyPacket).addListener(new GenericFutureListener<Future<? super Void>>()
        {

            @Override
            public void operationComplete(Future<? super Void> future) throws Exception
            {
                if(!future.isSuccess())
                {
                    future.cause().printStackTrace();
                }
            }
        });
    }
}
