/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.network;

import io.netty.buffer.*;

public class NettyPacket
{

    ByteBuf     payload;
    int         id;
    NetworkSide side;

    NettyPacket()
    {

    }

    public NettyPacket(int id, ByteBuf payload, NetworkSide side)
    {
        this.side = side;
        this.id = id;
        this.payload = payload;
    }

    public int getID()
    {
        return id;
    }

    public ByteBuf getPayload()
    {
        return payload;
    }

    public NetworkSide getSide()
    {
        return side;
    }
}
