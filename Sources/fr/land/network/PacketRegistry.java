/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.network;

import java.util.HashMap;

import fr.land.utils.Log;

public class PacketRegistry
{

	private static HashMap<NetworkSide, HashMap<Integer, Class<? extends AbstractPacket>>> packets;

	private static HashMap<Class<? extends AbstractPacket>, NetworkSide> sides;
	private static HashMap<Class<? extends AbstractPacket>, Integer> ids;

	public static void init()
	{
		PacketRegistry.sides = new HashMap<Class<? extends AbstractPacket>, NetworkSide>();
		PacketRegistry.ids = new HashMap<Class<? extends AbstractPacket>, Integer>();
		PacketRegistry.packets = new HashMap<NetworkSide, HashMap<Integer, Class<? extends AbstractPacket>>>();
		PacketRegistry.packets.put(NetworkSide.CLIENT, new HashMap<Integer, Class<? extends AbstractPacket>>());
		PacketRegistry.packets.put(NetworkSide.SERVER, new HashMap<Integer, Class<? extends AbstractPacket>>());

	}

	public static int getPacketId(Class<? extends AbstractPacket> packet)
	{
		return PacketRegistry.ids.get(packet);
	}

	public static NetworkSide getPacketSide(Class<? extends AbstractPacket> packet)
	{
		return PacketRegistry.sides.get(packet);
	}

	public static void registerPacket(NetworkSide senderSide, int id, Class<? extends AbstractPacket> packetClass)
	{
		PacketRegistry.packets.get(senderSide).put(id, packetClass);

		PacketRegistry.sides.put(packetClass, senderSide);
		PacketRegistry.ids.put(packetClass, id);
	}

	public static AbstractPacket create(NetworkSide side, int id)
	{
		Class<? extends AbstractPacket> packet = PacketRegistry.packets.get(side).get(id);
		if(packet == null)
			Log.fatal("Unknown packet id: " + id);
		try
		{
			return packet.newInstance();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
