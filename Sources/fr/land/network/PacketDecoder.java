/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.network;

import io.netty.buffer.*;
import io.netty.channel.*;
import io.netty.handler.codec.*;

import java.util.*;

public class PacketDecoder extends ByteToMessageDecoder
{

    private ByteBuf     buffer;
    private int         waitingPayloadSize;
    private NettyPacket packet;

    public PacketDecoder()
    {
        waitingPayloadSize = -1;
        buffer = Unpooled.buffer();
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf data, List<Object> out) throws Exception
    {
        buffer.writeBytes(data);
        if(waitingPayloadSize >= 0)
        {
            if(buffer.readableBytes() < waitingPayloadSize)
                return;
            ByteBuf payload = buffer.readBytes(Math.min(buffer.readableBytes(), waitingPayloadSize));
            ByteBuf payloadData = payload.readBytes(waitingPayloadSize);
            packet.payload = payloadData;
            out.add(packet);
            packet = null;
            waitingPayloadSize = -1;
            decode(ctx, payload, out);
        }
        else
        {
            if(buffer.readableBytes() >= 12)
            {
                packet = new NettyPacket();
                packet.id = buffer.readInt();
                packet.side = NetworkSide.values()[buffer.readInt()];
                int payloadSize = buffer.readInt();
                if(buffer.readableBytes() < payloadSize)
                {
                    waitingPayloadSize = payloadSize;
                }
                else
                {
                    ByteBuf payload = buffer.readBytes(payloadSize);
                    packet.payload = payload;
                    out.add(packet);
                    waitingPayloadSize = -1;
                }
            }
        }
    }
}
