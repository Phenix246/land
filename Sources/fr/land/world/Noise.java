/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.world;

import java.util.Random;

import fr.land.utils.math.Vector2f;

public class Noise
{
	private long seed;
	private Random rand;
	private int octave;
	private float amplitude;
	
	// ********************************************************************************************************************************************

	public Noise(long seed, int octave, float amplitude)
	{
		this.seed = seed;
		this.octave = octave;
		this.amplitude = amplitude;
		
		this.rand = new  Random();
		
	}
	
	// ********************************************************************************************************************************************

	public float getNoise( float x, float z)
	{
		int xmin = (int) ((double) x / this.octave);
		int xmax = xmin + 1;
		int zmin = (int) ((double) z / this.octave);
		int zmax = zmin + 1;

		Vector2f a = new Vector2f(xmin, zmin);
		Vector2f b = new Vector2f(xmax, zmin);
		Vector2f c = new Vector2f(xmax, zmax);
		Vector2f d = new Vector2f(xmin, zmax);
		
		float ra = (float)this.noise(a);
		float rb = (float)this.noise(b);
		float rc = (float)this.noise(c);
		float rd = (float)this.noise(d);
		
		float t1 = (x - xmin * this.octave) / this.octave;
		float t2 = (z - zmin * this.octave) / this.octave;
		float i1 = this.interpolate(ra, rb, t1);
		float i2 = this.interpolate(rd, rc, t1);
		
		float h = this.interpolate(i1, i2, t2);
		
		return h * this.amplitude;
	}
	
	// ********************************************************************************************************************************************

	private float interpolate(float a, float b, float t) {
		float ft = (float) (t*Math.PI);
		float f = (float) ((1f - Math.cos(ft)) * 0.5f);
		return a*(1f-f) + b*f;
	}
	
	private double noise(Vector2f coord) {
		double var = 10000 * (Math.sin(coord.x + Math.cos(coord.y)) + Math.tan(this.seed));
		this.rand.setSeed((long) var);
		return this.rand.nextDouble();
	}
}
