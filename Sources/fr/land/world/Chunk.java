/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.world;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

import fr.land.blocks.Block;
import fr.land.blocks.BlockList;
import fr.land.core.Land;

public class Chunk
{
	public static final int SIZE = 16;

	private int vbo, cubeVbo, textureVbo;
	private static FloatBuffer buffer, cubeBuffer, textureBuffer;
	private int bufferSize;

	private World world;
	private Block[][][] blocks;
	private int x, y, z;

	private Noise noise;

	public boolean isDirty = false;

	// ********************************************************************************************************************************************

	public Chunk(World world, int x, int y, int z, Noise noise)
	{
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.noise = noise;

		this.blocks = new Block[Chunk.SIZE][Chunk.SIZE][Chunk.SIZE];

		this.generateChunk();
	}

	// ********************************************************************************************************************************************

	private void generateChunk()
	{
		for(int x = 0; x < Chunk.SIZE; x++)
			for(int y = 0; y < Chunk.SIZE; y++)
				for(int z = 0; z < Chunk.SIZE; z++)
				{
					int xx = this.x * Chunk.SIZE + x;
					int yy = this.y * Chunk.SIZE + y;
					int zz = this.z * Chunk.SIZE + z;

					if(this.noise.getNoise(xx, zz) > yy - (16 + 4))
					{
						Block block = BlockList.stone;
						this.blocks[x][y][z] = block;
					}
				}
	}

	private void createBuffer()
	{
		this.vbo = GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, this.vbo);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, Chunk.buffer, GL15.GL_STATIC_DRAW);
	}

	public void createChunkVBO()
	{
		// largeur * longeur * hauteur * nb de face * (4 vectex pour une face) *
		// ( coor(x,y,z) + color(r,g,b,a)
		Chunk.buffer = BufferUtils.createFloatBuffer(Chunk.SIZE * Chunk.SIZE * Chunk.SIZE * 6 * 4 * (3 + 4));

		for(int x = 0; x < Chunk.SIZE; x++)
			for(int y = 0; y < Chunk.SIZE; y++)
				for(int z = 0; z < Chunk.SIZE; z++)
				{
					// Don't render block we can't see
					int xx = this.x * Chunk.SIZE + x;
					int yy = this.y * Chunk.SIZE + y;
					int zz = this.z * Chunk.SIZE + z;

					boolean up = this.world.getBlock(xx, yy + 1, zz) == null;
					boolean down = this.world.getBlock(xx, yy - 1, zz) == null;
					boolean left = this.world.getBlock(xx - 1, yy, zz) == null;
					boolean right = this.world.getBlock(xx + 1, yy, zz) == null;
					boolean front = this.world.getBlock(xx, yy, zz - 1) == null;
					boolean back = this.world.getBlock(xx, yy, zz + 1) == null;

					if(!up && !down && !left && !right && !front && !back)
						continue;

					if(this.blocks[x][y][z] == null)
						continue;

					Block block = this.blocks[x][y][z];

					// Don't render the side we don't see
					int size = 0;
					if(up)
					{
						Chunk.buffer.put(block.blockDataUp(xx, yy, zz));
						size++;
					}
					if(down)
					{
						Chunk.buffer.put(block.blockDataDown(xx, yy, zz));
						size++;
					}
					if(left)
					{
						Chunk.buffer.put(block.blockDataLeft(xx, yy, zz));
						size++;
					}
					if(right)
					{
						Chunk.buffer.put(block.blockDataRight(xx, yy, zz));
						size++;
					}
					if(front)
					{
						Chunk.buffer.put(block.blockDataFront(xx, yy, zz));
						size++;
					}
					if(back)
					{
						Chunk.buffer.put(block.blockDataBack(xx, yy, zz));
						size++;
					}

					this.bufferSize += size * 4;

				}
		Chunk.buffer.flip();
		this.createBuffer();
	}

	// ********************************************************************************************************************************************

	public void update(int tick)
	{
		if(this.isDirty)
		{
			this.createChunkVBO();
			this.isDirty = false;
		}

	}

	public void render()
	{
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);

		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, this.vbo);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 7 * 4, 0);
		GL20.glVertexAttribPointer(1, 4, GL11.GL_FLOAT, false, 7 * 4, 12);

		GL11.glDrawArrays(GL11.GL_QUADS, 0, this.bufferSize);

		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);

		if(!Land.getLand().isDebug)
			return;

		// chunk border
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		GL11.glDisable(GL11.GL_CULL_FACE);

		GL11.glLineWidth(1);
		GL11.glBegin(GL11.GL_QUADS);
		{
			GL11.glColor3f(1f, 0f, 0f);
			GL11.glVertex3f(this.x * Chunk.SIZE, this.y * Chunk.SIZE, this.z * Chunk.SIZE);
			GL11.glVertex3f(this.x * Chunk.SIZE + Chunk.SIZE, this.y * Chunk.SIZE, this.z * Chunk.SIZE);
			GL11.glVertex3f(this.x * Chunk.SIZE + Chunk.SIZE, this.y * Chunk.SIZE + Chunk.SIZE, this.z * Chunk.SIZE);
			GL11.glVertex3f(this.x * Chunk.SIZE, this.y * Chunk.SIZE + Chunk.SIZE, this.z * Chunk.SIZE);

			GL11.glVertex3f(this.x * Chunk.SIZE, this.y * Chunk.SIZE + Chunk.SIZE, this.z * Chunk.SIZE);
			GL11.glVertex3f(this.x * Chunk.SIZE + Chunk.SIZE, this.y * Chunk.SIZE + Chunk.SIZE, this.z * Chunk.SIZE);
			GL11.glVertex3f(this.x * Chunk.SIZE + Chunk.SIZE, this.y * Chunk.SIZE + Chunk.SIZE, this.z * Chunk.SIZE + Chunk.SIZE);
			GL11.glVertex3f(this.x * Chunk.SIZE, this.y * Chunk.SIZE + Chunk.SIZE, this.z * Chunk.SIZE + Chunk.SIZE);
		}
		GL11.glEnd();

		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
	}

	// ********************************************************************************************************************************************

	public Block getBlock(int x, int y, int z)
	{
		if(x < 0 || y < 0 || z < 0 || x >= Chunk.SIZE || y >= Chunk.SIZE || z >= Chunk.SIZE)
			return null;
		return this.blocks[x][y][z];
	}

	public void setBlock(int x, int y, int z, Block block)
	{
		if(x < 0 || y < 0 || z < 0 || x >= Chunk.SIZE || y >= Chunk.SIZE || z >= Chunk.SIZE)
			return;
		this.blocks[x][y][z] = block;
		this.isDirty = true;
	}
}
