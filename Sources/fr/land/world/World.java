/*******************************************************************************
 * Copyright (c) 2015, Phenix246
 *
 * This work is made available under the terms of the Creative Commons Attribution License:
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en
 * Contact the author for use the sources
 *
 * Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
 * Contacter l'auteur pour utiliser les sources
 *
 * Este trabajo está disponible bajo los términos de la licencia Creative Commons Atribución :
 * http://creativecommons.org/licenses/by-nc-sa/4.0/deed.es
 * Contactar al autor para utilizar las fuentes
 *
 ******************************************************************************/
package fr.land.world;

import java.util.ArrayList;
import java.util.Random;

import fr.land.blocks.Block;
import fr.land.blocks.BlockList;
import fr.land.entities.Entity;
import fr.land.utils.math.AxisAlignedBB;

public class World
{
	public static final int SIZE = 5;
	public static final int HEIGHT = 3;
	private Chunk[][][] chunks;
	private Noise noise;
	
	public Random random;

	public ArrayList<Entity> entitiesList;

	// ********************************************************************************************************************************************

	public World()
	{
		this.chunks = new Chunk[World.SIZE][World.HEIGHT][World.SIZE];
		this.noise = new Noise(new Random().nextLong(), 20, 10);
		this.random = new Random();
		this.entitiesList = new ArrayList<Entity>();

		for(int x = 0; x < World.SIZE; x++)
			for(int y = 0; y < World.HEIGHT; y++)
				for(int z = 0; z < World.SIZE; z++)
					this.chunks[x][y][z] = new Chunk(this, x, y, z, this.noise);

		for(int x = 0; x < World.SIZE; x++)
			for(int y = 0; y < World.HEIGHT; y++)
				for(int z = 0; z < World.SIZE; z++)
					this.chunks[x][y][z].createChunkVBO();

		for(int x = 15; x < 15 + 3; x++)
			for(int y = 30; y < 30 + 3; y++)
				for(int z = 30; z < 30 + 3; z++)
					this.setBlock(x, y, z, BlockList.stone);
	}

	// ********************************************************************************************************************************************

	public void update(int tick)
	{
		// Blocks
		this.updateBlocks(tick);
		
		// Entities
		this.updateEntities(tick);
	}

	public void render()
	{
		// Blocks
		this.renderBlocks();

		// Entities
		this.renderEntities();


	}

	// ********************************************************************************************************************************************

	public Chunk getChunk(int x, int y, int z)
	{
		int xc = x / Chunk.SIZE;
		int yc = y / Chunk.SIZE;
		int zc = z / Chunk.SIZE;

		if(xc < 0 || yc < 0 || zc < 0 || xc >= World.SIZE || yc >= World.HEIGHT || zc >= World.SIZE)
			return null;
		return this.chunks[xc][yc][zc];
	}

	// ********************************************************************************************************************************************

	public void updateBlocks(int tick)
	{
		for(int x = 0; x < World.SIZE; x++)
			for(int y = 0; y < World.HEIGHT; y++)
				for(int z = 0; z < World.SIZE; z++)
					this.chunks[x][y][z].update(tick);
	}

	public void renderBlocks()
	{
		for(int x = 0; x < World.SIZE; x++)
			for(int y = 0; y < World.HEIGHT; y++)
				for(int z = 0; z < World.SIZE; z++)
					this.chunks[x][y][z].render();
	}

	public Block getBlock(int x, int y, int z)
	{
		Chunk chunk = this.getChunk(x, y, z);

		int xb = x % Chunk.SIZE;
		int yb = y % Chunk.SIZE;
		int zb = z % Chunk.SIZE;

		return chunk != null ? chunk.getBlock(xb, yb, zb) : null;
	}

	public void setBlock(int x, int y, int z, Block block)
	{
		Chunk chunk = this.getChunk(x, y, z);
		if(chunk == null)
			return;

		int xb = x % Chunk.SIZE;
		int yb = y % Chunk.SIZE;
		int zb = z % Chunk.SIZE;

		chunk.setBlock(xb, yb, zb, block);
	}

	public void removeBlock(int x, int y, int z)
	{
		this.setBlock(x, y, z, null);
	}

	public AxisAlignedBB getBoundingBox(int x, int y, int z)
	{
		Block b = this.getBlock(x, y, z);
		if(b == null)
			return new AxisAlignedBB(x, y, z, 0, 0, 0);
		return b.getBoundingBox(x, y, z);
	}

	// ********************************************************************************************************************************************

	public void updateEntities(int tick)
	{
		for(Entity e : this.entitiesList)
			e.update(tick);
	}

	public void renderEntities()
	{
		for(Entity e : this.entitiesList)
			e.render();
	}

	public void addEntity(Entity e)
	{
		e.init(this);
		this.entitiesList.add(e);
	}

	public void removeEntity(Entity e)
	{
		this.entitiesList.remove(e);
	}

}
